<?php
include('config.php');
include('functies.php');

$gebruikers = gebruikers_ophalen();
if(sizeof($gebruikers) > 0){
	$stand_fles = aantal_fles_ophalen();	
}
?>



<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title>Bierlijst by Corry!</title>
		<!-- mobile viewport optimisation -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- stylesheets -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.css" type="text/css"/>
		<script src="jquery-1.11.3.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-4 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Biertjes doorvoeren</div>
					<div style="padding: 15px;">
						<form name="fles_mutatie" action="verwerk.php" method="post">
							<input type="hidden" name="fles_mutatie" value="1">

						<?php
						foreach($gebruikers as $gebruiker){
						?>
							<div class="row">
								<div class="col-md-2">
									<label for"flesInvoer<?=$gebruiker['gebruiker_id']?>"><?=$gebruiker['gebruiker_naam']?></label>
								</div>
								<div class="col-md-9">
									<input oninput="amount<?=$gebruiker['gebruiker_id']?>.value=flesInvoer<?=$gebruiker['gebruiker_id']?>.value" type="range" id="flesInvoer<?=$gebruiker['gebruiker_id']?>" Value="0" name="flesInvoer<?=$gebruiker['gebruiker_id']?>" min="0" max="10">
								</div>
								<div class="col-md-1">
									<output name="amount<?=$gebruiker['gebruiker_id']?>" for="flesInvoer<?=$gebruiker['gebruiker_id']?>">0</output>
								</div>
							</div>
						<?php
						}
						?>
							<button type="submit" class="btn btn-success">Doervoeren!</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Kratjes doorvoeren</div>
					<div style="padding: 15px;">
						<form name="krat_mutatie" action="verwerk.php" method="post">
							<input type="hidden" name="krat_mutatie" value="1">

						<?php
						foreach($gebruikers as $gebruiker){
						?>
							<div class="row">
								<div class="col-md-2">
									<label for"kratInvoer<?=$gebruiker['gebruiker_id']?>"><?=$gebruiker['gebruiker_naam']?></label>
								</div>
								<div class="col-md-9">
									<input oninput="amount<?=$gebruiker['gebruiker_id']?>.value=kratInvoer<?=$gebruiker['gebruiker_id']?>.value" type="range" id="kratInvoer<?=$gebruiker['gebruiker_id']?>" Value="0" name="kratInvoer<?=$gebruiker['gebruiker_id']?>" min="0" max="5">
								</div>
								<div class="col-md-1">
									<output name="amount<?=$gebruiker['gebruiker_id']?>" for="kratInvoer<?=$gebruiker['gebruiker_id']?>">0</output>
								</div>
							</div>
						<?php
						}
						?>
							<button type="submit" class="btn btn-success">Doervoeren!</button>
						</form>
				</div>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Instellingen</div>
					<div style="padding: 15px;">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Gebruiker toevoegen
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li style="padding: 6px;">
									<form name="gebruiker_toevoegen" action="gebruiker_toevoegen.php" method="POST">
										<div class="col-lg-12">
											<div class="input-group">
												<input type="text" class="form-control" name="gebruiker_naam_toevoegen" id="gebruiker_naam_toevoegen" width="100%">
												<span class="input-group-btn">
													<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
												</span>
											</div>
										</div>
									</form>
								</li>
							</ul>
						</div>
						<br />
						<br />
						<br />
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Gebruiker verwijderen
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<?php
								foreach($gebruikers as $gebruiker){
									echo '<li>';
										echo '<a href="gebruiker_verwijderen.php?id='.$gebruiker['gebruiker_id'].'">';
											echo $gebruiker['gebruiker_naam'];
										echo '<span class="glyphicon glyphicon-remove pull-right" aria-hidden="true"></a>';
									echo '</li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Tussenstand</div>
					<div style="padding: 15px;">
						<table class="table table-striped">
							<tr>
								<td>Plaats</td>
								<td>Naam</td>
								<td>Gedronken</td>
								<td>Gekocht</td>
								<td>Reserve</td>
							</tr>
						<?php
							if(sizeof($gebruikers) > 0){
								$stand_fles_totaal = 0;
								$stand_gekocht_totaal = 0;
								foreach($stand_fles as $key => $gebruiker){
									$gebruiker_naam_key = gebruiker_naam_ophalen($stand_fles[$key]['gebruiker_id']);
									$stand_fles_key = $stand_fles[$key]['aantal_fles'];
									$stand_fles_totaal += $stand_fles_key;
									$stand_gekocht_key = aantal_krat_ophalen($stand_fles[$key]['gebruiker_id'])*24;
									$stand_gekocht_totaal += $stand_gekocht_key;
								?>
									<tr>
										<td><?=$key?></td>
										<td><?=$gebruiker_naam_key?></td>
										<td><?=$stand_fles_key?></td>
										<td><?=$stand_gekocht_key?></td>
										<td><?=$stand_gekocht_key-$stand_fles_key?></td>
									</tr>
								<?php
								}
								?>
									<tr>
										<td></td>
										<td>Totaal</td>
										<td><?=$stand_fles_totaal?></td>
										<td><?=$stand_gekocht_totaal?></td>
										<td><?=$stand_gekocht_totaal-$stand_fles_totaal?></td>
									</tr>
							<?php
							}
							?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>