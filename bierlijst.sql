-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 07 jul 2015 om 04:24
-- Serverversie: 5.6.17
-- PHP-versie: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `bierlijst`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruikers`
--

CREATE TABLE IF NOT EXISTS `gebruikers` (
  `gebruiker_id` int(11) NOT NULL AUTO_INCREMENT,
  `gebruiker_naam` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `actief` tinyint(1) NOT NULL,
  PRIMARY KEY (`gebruiker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=15 ;

--
-- Gegevens worden geëxporteerd voor tabel `gebruikers`
--

INSERT INTO `gebruikers` (`gebruiker_id`, `gebruiker_naam`, `actief`) VALUES
(10, 'Michiel', 1),
(11, 'Guy', 1),
(12, 'Pondjes', 0),
(13, 'Boekelo', 1),
(14, 'Pondjes', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `mutatie_fles`
--

CREATE TABLE IF NOT EXISTS `mutatie_fles` (
  `mutatie_id` int(11) NOT NULL AUTO_INCREMENT,
  `gebruiker_id` int(11) NOT NULL,
  `mutatie` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mutatie_id`),
  KEY `gebruiker_id` (`gebruiker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=44 ;

--
-- Gegevens worden geëxporteerd voor tabel `mutatie_fles`
--

INSERT INTO `mutatie_fles` (`mutatie_id`, `gebruiker_id`, `mutatie`, `timestamp`) VALUES
(43, 10, 6, '2015-07-07 02:22:32');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `mutatie_krat`
--

CREATE TABLE IF NOT EXISTS `mutatie_krat` (
  `mutatie_id` int(11) NOT NULL AUTO_INCREMENT,
  `gebruiker_id` int(11) NOT NULL,
  `mutatie` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mutatie_id`),
  KEY `gebruiker_id` (`gebruiker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=99 ;

--
-- Gegevens worden geëxporteerd voor tabel `mutatie_krat`
--

INSERT INTO `mutatie_krat` (`mutatie_id`, `gebruiker_id`, `mutatie`, `timestamp`) VALUES
(98, 10, 1, '2015-07-07 02:22:36');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `stand_fles`
--

CREATE TABLE IF NOT EXISTS `stand_fles` (
  `gebruiker_id` int(11) NOT NULL,
  `aantal_fles` int(11) NOT NULL,
  PRIMARY KEY (`gebruiker_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Gegevens worden geëxporteerd voor tabel `stand_fles`
--

INSERT INTO `stand_fles` (`gebruiker_id`, `aantal_fles`) VALUES
(10, 6),
(11, 0),
(12, 0),
(13, 0),
(14, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `stand_krat`
--

CREATE TABLE IF NOT EXISTS `stand_krat` (
  `gebruiker_id` int(11) NOT NULL,
  `aantal_krat` int(11) NOT NULL,
  PRIMARY KEY (`gebruiker_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Gegevens worden geëxporteerd voor tabel `stand_krat`
--

INSERT INTO `stand_krat` (`gebruiker_id`, `aantal_krat`) VALUES
(10, 1),
(11, 0),
(12, 0),
(13, 0),
(14, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
