<?php

$gebruiker_id = isset($_GET['id']) ? $_GET['id'] : false;
include('config.php');
include('functies.php');

if($gebruiker_id != false) {
	gebruiker_verwijderen($gebruiker_id);
	header("location: index.php");
}

?>